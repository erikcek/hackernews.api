import dotenv from "dotenv";
import Koa from "koa";
import Router from "koa-router";
import logger from "koa-logger";
import cors from "@koa/cors";
import bodyParser from "koa-bodyparser";
import { RegisterRoutes } from "./routes/routes";
import { koaSwagger } from "koa2-swagger-ui";
import * as swaggerDocument from "./swagger.v1.json";
import { ValidateError } from "@tsoa/runtime";
import { connect as pgConnect } from "./pg_sequelize";
import cron from "node-cron";
import {
  removeUnusedArticlesWithComments,
  syncHanckerNewsArticlesJob,
} from "./crons/sync.cron";
import * as winstonLogger from "./helpers/logger";
import { HttpError } from "./helpers/error";

export const createServer = () => {
  const winstonInstance = winstonLogger.getInstance();
  //inicializing koa application
  const port = 9999;
  const app = new Koa();
  const router = new Router();

  app.use(cors());
  app.use(bodyParser());

  // swagger-ui documentatin from auto.generated swagger.json
  app.use(
    koaSwagger({
      routePrefix: "/v1/docs",
      swaggerOptions: { spec: swaggerDocument },
    })
  );

  // servig swagger.json for possible SDK generation etc.
  router.get("/swagger", async (ctx) => {
    ctx.body = swaggerDocument;
  });

  app.use(async (ctx, next) => {
    try {
      await next();
    } catch (err) {
      if (err instanceof ValidateError) {
        ctx.status = 422;
        ctx.body = {
          status: 422,
          message: "Validation Error",
          description: "Input schema validation error",
          payload: err?.fields,
        };
        winstonInstance?.error(err);
        ctx.app.emit("error", err, ctx);

        // custom error handling for custom error classes (helpers/error.ts)
        if (err instanceof HttpError) {
          console.log(err);

          ctx.status = err.statusCode || 500;
          ctx.body = {
            status: err.statusCode,
            message: err.statusMessage,
            description: err.description,
          };
          if (process.env.NODE_ENV === "dev") {
            ctx.body.payload = err.stack;
          }
          winstonInstance?.error({ ...err, payload: err.stack });
          ctx.app.emit("error", err, ctx);
        }
      } else {
        ctx.status = err.statusCode;
        ctx.body = {
          status: err.status,
          message: err.message,
          description: err.message,
        };
        if (process.env.NODE_ENV === "dev") {
          ctx.body.payload = err.stack;
        }
        winstonInstance?.error({ ...err, payload: err.stack });
        ctx.app.emit("error", err, ctx);
      }
    }
  });

  // registering generated tsoa routes ( routes/routes.ts )
  RegisterRoutes(router);

  app.use(
    logger((str, args) => {
      winstonLogger.getInstance()?.info(str);
    })
  );
  app.use(router.routes()).use(router.allowedMethods());

  const server = app.listen(port);
  return server;
};
