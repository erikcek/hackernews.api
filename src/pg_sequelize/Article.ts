import * as Sequelize from "sequelize";
import { HNArticle } from "../typings/CustomTsoa";
import { SequelizeAttributes } from "../typings/SequelizeAttributes";
import { HNCollectionInstance } from "./Collection";
import { HNCommentAttributes, HNCommentInstance } from "./Comment";

// Articles Sequelize

export interface HNArticleAttributes
  extends Omit<HNArticle, "id" | "createdAt" | "updatedAt"> {
  id?: number;
  hn_id: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface HNArticleInstance
  extends Sequelize.Instance<HNArticleAttributes>,
    HNArticleAttributes {
  getComments: Sequelize.HasManyGetAssociationsMixin<HNCommentInstance>;
  setComments: Sequelize.HasManySetAssociationsMixin<
    HNCommentInstance,
    HNCommentInstance["id"]
  >;
  addComments: Sequelize.HasManyAddAssociationsMixin<
    HNCommentInstance,
    HNCommentInstance["id"]
  >;
  addComment: Sequelize.HasManyAddAssociationMixin<
    HNCommentInstance,
    HNCommentInstance["id"]
  >;
  createComment: Sequelize.HasManyCreateAssociationMixin<
    HNCommentAttributes,
    HNCommentInstance
  >;
  removeComment: Sequelize.HasManyRemoveAssociationMixin<
    HNCommentInstance,
    HNCommentInstance["id"]
  >;
  removeComments: Sequelize.HasManyRemoveAssociationsMixin<
    HNCommentInstance,
    HNCommentInstance["id"]
  >;
  hasComment: Sequelize.HasManyHasAssociationMixin<
    HNCommentInstance,
    HNCommentInstance["id"]
  >;
  hasComments: Sequelize.HasManyHasAssociationsMixin<
    HNCommentInstance,
    HNCommentInstance["id"]
  >;
  countComments: Sequelize.HasManyCountAssociationsMixin;

  hasCollection: Sequelize.HasManyHasAssociationMixin<
    HNCollectionInstance,
    HNCollectionInstance["id"]
  >;
  hasCollections: Sequelize.HasManyHasAssociationsMixin<
    HNCollectionInstance,
    HNCollectionInstance["id"]
  >;
  getCollections: Sequelize.HasManyGetAssociationsMixin<HNCollectionInstance>;
  setCollections: Sequelize.HasManySetAssociationsMixin<
    HNCollectionInstance,
    HNCollectionInstance["id"]
  >;
  removeCollection: Sequelize.HasManyRemoveAssociationMixin<
    HNCollectionInstance,
    HNCollectionInstance["id"]
  >;
  removeCollections: Sequelize.HasManyRemoveAssociationsMixin<
    HNCollectionInstance,
    HNCollectionInstance["id"]
  >;
}

export const ArticleFactory = (
  sequelize: Sequelize.Sequelize,
  DataTypes: Sequelize.DataTypes
): Sequelize.Model<HNArticleInstance, HNArticleAttributes> => {
  const attributes: SequelizeAttributes<HNArticleAttributes> = {
    title: {
      type: DataTypes.STRING,
    },
    text: {
      type: DataTypes.STRING(10000),
    },
    author: {
      type: DataTypes.STRING,
    },
    hn_id: {
      type: DataTypes.INTEGER,
    },
    url: {
      type: DataTypes.STRING,
    },
  };

  const Article = sequelize.define<HNArticleInstance, HNArticleAttributes>(
    "Article",
    attributes
  );

  Article.associate = (models) => {
    Article.belongsToMany(models.Collection, { through: "Article_Collection" });
    Article.hasMany(models.Comment);
  };

  return Article;
};
