import { Sequelize } from "sequelize";
import { DbInterface } from "../typings/DbInterface";
import { UserFactory } from "./User";
import { ArticleFactory } from "./Article";
import { CommentFactory } from "./Comment";
import { CollectionFactory } from "./Collection";

const dbState: { state?: DbInterface; sequelizeInstance?: Sequelize } = {
  state: undefined,
};

export const createModels = (
  sequelizeConfig: any
): [DbInterface, Sequelize] => {
  const { database, username, password, params } = sequelizeConfig;
  const sequelize = new Sequelize(database, username, password, params);

  const db: DbInterface = {
    sequelize,
    Sequelize,
    Comment: CommentFactory(sequelize, Sequelize),
    Article: ArticleFactory(sequelize, Sequelize),
    User: UserFactory(sequelize, Sequelize),
    Collection: CollectionFactory(sequelize, Sequelize),
  };

  (Object.keys(db) as Array<keyof typeof db>).map((item) => {
    if ((db[item] as any).associate) {
      (db[item] as any).associate(db);
    }
  });

  return [db, sequelize];
};

// config is json config file
export const connect = async (config: any) => {
  if (dbState.state === undefined) {
    const [db, sequelize] = createModels(config);
    dbState.state = db;
    dbState.sequelizeInstance = sequelize;
    await dbState.state.sequelize.sync();
    return dbState.state;
  }
};

export const getInstance = () => {
  return dbState.state;
};

export const closeConnection = async () => {
  await dbState.sequelizeInstance?.close();
};
