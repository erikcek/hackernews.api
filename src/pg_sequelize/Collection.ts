import * as Sequelize from "sequelize";
import { HNCollection } from "../typings/CustomTsoa";
import { SequelizeAttributes } from "../typings/SequelizeAttributes";
import { HNArticleAttributes, HNArticleInstance } from "./Article";

// Collection Sequelize

export interface HNCollectionAttributes
  extends Omit<HNCollection, "id" | "owner" | "createdAt" | "updatedAt"> {
  id?: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface HNCollectionInstance
  extends Sequelize.Instance<HNCollectionAttributes>,
    HNCollectionAttributes {
  getArticles: Sequelize.HasManyGetAssociationsMixin<HNArticleInstance>;
  setArticles: Sequelize.HasManySetAssociationsMixin<
    HNArticleInstance,
    HNArticleInstance["id"]
  >;
  addArticles: Sequelize.HasManyAddAssociationsMixin<
    HNArticleInstance,
    HNArticleInstance["id"]
  >;
  addArticle: Sequelize.HasManyAddAssociationMixin<
    HNArticleInstance,
    HNArticleInstance["id"]
  >;
  createArticle: Sequelize.HasManyCreateAssociationMixin<
    HNArticleAttributes,
    HNArticleInstance
  >;
  removeArticle: Sequelize.HasManyRemoveAssociationMixin<
    HNArticleInstance,
    HNArticleInstance["id"]
  >;
  removeArticles: Sequelize.HasManyRemoveAssociationsMixin<
    HNArticleInstance,
    HNArticleInstance["id"]
  >;
  hasArticle: Sequelize.HasManyHasAssociationMixin<
    HNArticleInstance,
    HNArticleInstance["id"]
  >;
  hasArticles: Sequelize.HasManyHasAssociationsMixin<
    HNArticleInstance,
    HNArticleInstance["id"]
  >;
  countArticles: Sequelize.HasManyCountAssociationsMixin;
}

export const CollectionFactory = (
  sequelize: Sequelize.Sequelize,
  DataTypes: Sequelize.DataTypes
): Sequelize.Model<HNCollectionInstance, HNCollectionAttributes> => {
  const attributes: SequelizeAttributes<HNCollectionAttributes> = {
    name: {
      type: DataTypes.STRING,
    },
  };

  const Collection = sequelize.define<
    HNCollectionInstance,
    HNCollectionAttributes
  >("Collection", attributes);

  Collection.associate = (models) => {
    Collection.belongsToMany(models.Article, { through: "Article_Collection" });
    Collection.belongsTo(models.User);
  };

  return Collection;
};
