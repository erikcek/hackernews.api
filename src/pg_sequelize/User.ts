import Sequelize from "sequelize";
import { User } from "../typings/CustomTsoa";
import { SequelizeAttributes } from "../typings/SequelizeAttributes";
import { HNCollectionAttributes, HNCollectionInstance } from "./Collection";

// User Sequelize

export interface UserAttributes
  extends Omit<User, "id" | "createdAt" | "updatedAt"> {
  id?: number;
  password: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface UserInstance
  extends Sequelize.Instance<UserAttributes>,
    UserAttributes {
  getCollections: Sequelize.HasManyGetAssociationsMixin<HNCollectionInstance>;
  setCollections: Sequelize.HasManySetAssociationsMixin<
    HNCollectionInstance,
    HNCollectionInstance["id"]
  >;
  addCollections: Sequelize.HasManyAddAssociationsMixin<
    HNCollectionInstance,
    HNCollectionInstance["id"]
  >;
  addCollection: Sequelize.HasManyAddAssociationMixin<
    HNCollectionInstance,
    HNCollectionInstance["id"]
  >;
  createCollection: Sequelize.HasManyCreateAssociationMixin<
    HNCollectionAttributes,
    HNCollectionInstance
  >;
  removeCollection: Sequelize.HasManyRemoveAssociationMixin<
    HNCollectionInstance,
    HNCollectionInstance["id"]
  >;
  removeCollections: Sequelize.HasManyRemoveAssociationsMixin<
    HNCollectionInstance,
    HNCollectionInstance["id"]
  >;
  hasCollection: Sequelize.HasManyHasAssociationMixin<
    HNCollectionInstance,
    HNCollectionInstance["id"]
  >;
  hasCollections: Sequelize.HasManyHasAssociationsMixin<
    HNCollectionInstance,
    HNCollectionInstance["id"]
  >;
  countComments: Sequelize.HasManyCountAssociationsMixin;
}

export const UserFactory = (
  sequelize: Sequelize.Sequelize,
  DataTypes: Sequelize.DataTypes
): Sequelize.Model<UserInstance, UserAttributes> => {
  const attributes: SequelizeAttributes<UserAttributes> = {
    name: {
      type: DataTypes.STRING,
    },
    password: {
      type: DataTypes.STRING,
    },
  };

  const User = sequelize.define<UserInstance, UserAttributes>(
    "User",
    attributes
  );

  User.associate = (models) => {
    User.hasMany(models.Collection, { foreignKey: "UserId" });
  };

  return User;
};
