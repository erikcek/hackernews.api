import * as Sequelize from "sequelize";
import { HNComment } from "../typings/CustomTsoa";
import { SequelizeAttributes } from "../typings/SequelizeAttributes";

// Comments Sequelize

export interface HNCommentAttributes extends Omit<HNComment, "id"> {
  id?: number;
  hn_id: number;
  parent_id?: number;
}

export interface HNCommentInstance
  extends Sequelize.Instance<HNCommentAttributes>,
    HNCommentAttributes {}

export const CommentFactory = (
  sequelize: Sequelize.Sequelize,
  DataTypes: Sequelize.DataTypes
): Sequelize.Model<HNCommentInstance, HNCommentAttributes> => {
  const attributes: SequelizeAttributes<HNCommentAttributes> = {
    text: {
      type: DataTypes.TEXT(),
    },
    kids: {
      type: DataTypes.ARRAY(DataTypes.INTEGER),
    },
    hn_id: {
      type: DataTypes.INTEGER(),
    },
    parent_id: {
      type: DataTypes.INTEGER(),
    },
  };

  const Comment = sequelize.define<HNCommentInstance, HNCommentAttributes>(
    "Comment",
    attributes
  );

  Comment.associate = (models) => {
    Comment.belongsTo(models.Article, {
      constraints: false,
    });
  };

  return Comment;
};
