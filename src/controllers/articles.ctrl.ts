import {
  Body,
  Controller,
  Delete,
  Get,
  Path,
  Post,
  Request,
  Response,
  Route,
  Security,
  SuccessResponse,
  Tags,
} from "tsoa";
import {
  ErrorResponse,
  HNArticle,
  HNArticleCreate,
  HNArticleList,
  KoaRequestWithUser,
} from "../typings/CustomTsoa";

import * as collectionService from "../services/collection.service";
import * as hackerNewsService from "../services/hackerNews.service";
import * as articleService from "../services/article.service";
import * as commentService from "../services/comment.service";
import {
  ForbiddenError,
  NotFoundError,
  UnathorizedError,
} from "../helpers/error";
import { getInstance } from "../helpers/logger";

@Route("/collections/{collectionId}")
@Tags("articles")
export class ArticleController extends Controller {
  @Get("/articles")
  @SuccessResponse("200")
  @Response<ErrorResponse>(401, UnathorizedError.displayName)
  @Response<ErrorResponse>(403, ForbiddenError.displayName)
  @Security("jwt", ["article"])
  public async getCollectionArticles(
    @Request() request: KoaRequestWithUser,
    @Path() collectionId: number
  ): Promise<HNArticleList> {
    await collectionService.verifyCollectionOwnership(
      collectionId,
      request.user.sub
    );
    const articles = (await articleService.getArticles(
      collectionId
    )) as HNArticle[];
    return { articles: articles };
  }

  @Post("/articles")
  @SuccessResponse("202", "Accepted")
  @Response<ErrorResponse>(401, UnathorizedError.displayName)
  @Response<ErrorResponse>(403, ForbiddenError.displayName)
  @Security("jwt", ["article"])
  public async createCollectionArticle(
    @Request() request: KoaRequestWithUser,
    @Path() collectionId: number,
    @Body() body: HNArticleCreate
  ): Promise<{}> {
    await collectionService.verifyCollectionOwnership(
      collectionId,
      request.user.sub
    );
    const fetchArticlesAndComments = async () => {
      try {
        await Promise.all(
          body.article_ids.map(async (articleId) => {
            const article = await hackerNewsService.getStory(articleId);
            const localArticle = (await articleService.createArticle(
              article,
              collectionId
            )) as HNArticle;
            if (article.comments) {
              const comments = await hackerNewsService.getMultipleComments(
                article.comments
              );
              await commentService.createMultipleComments(
                collectionId,
                request.user.sub,
                localArticle.id,
                comments
              );
            }
          })
        );
      } catch (error) {
        getInstance()?.error({
          message: "[ARTICLE FETCH] fetching article with comments",
          payload: error,
        });
      }
    };
    // api returns 202 - Accepted with no further resposne
    // therefore is function not awaited, just registered
    fetchArticlesAndComments();
    this.setStatus(202);
    return {};
  }

  @Get("/articles/{articleId}")
  @SuccessResponse("200", "OK")
  @Response<ErrorResponse>(401, UnathorizedError.displayName)
  @Response<ErrorResponse>(404, NotFoundError.displayName)
  @Response<ErrorResponse>(403, ForbiddenError.displayName)
  @Security("jwt", ["article"])
  public async getCollectionArticle(
    @Request() request: KoaRequestWithUser,
    @Path() collectionId: number,
    @Path() articleId: number
  ): Promise<HNArticle> {
    await collectionService.verifyCollectionOwnership(
      collectionId,
      request.user.sub
    );
    const article = await articleService.getArticle(articleId);
    return article as HNArticle;
  }

  // removes article from collection association
  // in case article does not have any associated collections ->
  // will be cleand up by cron (see cron directory)
  @Delete("/articles/{articleId}")
  @SuccessResponse("200", "Deleted")
  @Response<ErrorResponse>(401, UnathorizedError.displayName)
  @Response<ErrorResponse>(404, NotFoundError.displayName)
  @Response<ErrorResponse>(403, ForbiddenError.displayName)
  @Security("jwt", ["article"])
  public async removeCollectionArticle(
    @Request() request: KoaRequestWithUser,
    @Path() collectionId: number,
    @Path() articleId: number
  ): Promise<{}> {
    await collectionService.verifyCollectionOwnership(
      collectionId,
      request.user.sub
    );
    await articleService.removeArticleFromCollection(articleId, collectionId);
    return {};
  }
}
