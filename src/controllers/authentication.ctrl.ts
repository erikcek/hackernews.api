import jwt from "jsonwebtoken";
import { Request } from "koa";
import { UnathorizedError } from "../helpers/error";
import { getInstance } from "../helpers/logger";

export const koaAuthentication = async (
  request: Request,
  securityName: string,
  scopes?: string[]
): Promise<any> => {
  try {
    if (securityName === "jwt") {
      const token = request.headers["authorization"];
      if (!token) {
        throw new UnathorizedError("No token provided");
      }
      const splittedToken = token?.split(" ");
      if (splittedToken?.length != 2) {
        throw new UnathorizedError("Invalid access token");
      }
      const jwtToken = (splittedToken as string[])[1];
      try {
        const decoded = jwt.verify(
          jwtToken,
          process.env.JWT_SECRET as string
        ) as any;

        if (scopes) {
          const verifiedScopes = scopes.reduce(
            (acum: boolean, item: string) => {
              if (!acum) {
                return false;
              }
              if (decoded.scopes.includes(item)) {
                return true;
              }
              return false;
            },
            true
          );
          if (!verifiedScopes) {
            throw new UnathorizedError("Not permited for this operation");
          }
        }
        return decoded;
      } catch (error) {
        getInstance()?.error(error);
        throw new UnathorizedError("Invalid access token");
      }
    }
  } catch (error) {
    getInstance()?.error(error);
    throw new UnathorizedError("There was a problem with authorization");
  }
};
