import {
  Body,
  Controller,
  Post,
  Request,
  Response,
  Route,
  SuccessResponse,
  Tags,
} from "tsoa";
import {
  ErrorResponse,
  User,
  UserLoginBody,
  UserLoginResponse,
} from "../typings/CustomTsoa";
import koa from "koa";
import * as userService from "../services/user.service";
import { UnathorizedError } from "../helpers/error";

@Route("users")
@Tags("users")
export class UserController extends Controller {
  @SuccessResponse("200")
  @Post("/signUp")
  public async signUp(
    @Request() request: koa.Request,
    @Body() body: UserLoginBody
  ): Promise<User> {
    return await userService.signUp(body);
  }

  @SuccessResponse("200")
  @Post("/authorize")
  @Response<ErrorResponse>(401, UnathorizedError.displayName)
  public async authorize(
    @Request() request: koa.Request,
    @Body() body: UserLoginBody
  ): Promise<UserLoginResponse> {
    return await userService.authorize(body);
  }
}
