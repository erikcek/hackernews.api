import {
  Body,
  Controller,
  Delete,
  Get,
  Path,
  Post,
  Put,
  Request,
  Response,
  Route,
  Security,
  SuccessResponse,
  Tags,
} from "tsoa";
import {
  HNCollectionCreate,
  HNCollectionList,
  HNCollection,
  KoaRequestWithUser,
  ErrorResponse,
} from "../typings/CustomTsoa";

import * as collectionService from "../services/collection.service";
import { HNCollectionAttributes } from "../pg_sequelize/Collection";
import {
  NotFoundError,
  UnathorizedError,
  ForbiddenError,
} from "../helpers/error";

@Route("collections")
@Tags("collections")
export class CollectionController extends Controller {
  @Get("/")
  @SuccessResponse("200")
  @Response<ErrorResponse>(401, UnathorizedError.displayName)
  @Security("jwt", ["collection"])
  public async getCollections(
    @Request() request: KoaRequestWithUser
  ): Promise<HNCollectionList> {
    const collections = (await collectionService.getCollections(
      request.user.sub
    )) as HNCollection[];
    return <HNCollectionList>{ collections };
  }

  @Post("/")
  @SuccessResponse("201", "Created")
  @Response<ErrorResponse>(401, UnathorizedError.displayName)
  @Security("jwt", ["collection"])
  public async createCollection(
    @Request() request: KoaRequestWithUser,
    @Body() body: HNCollectionCreate
  ): Promise<HNCollection> {
    const newCollection = await collectionService.createCollection(
      body as HNCollectionAttributes,
      request.user.sub
    );
    this.setStatus(201);
    return newCollection as HNCollection;
  }

  @Get("/{id}")
  @SuccessResponse("200")
  @Response<ErrorResponse>(401, UnathorizedError.displayName)
  @Response<ErrorResponse>(404, NotFoundError.displayName)
  @Response<ErrorResponse>(403, ForbiddenError.displayName)
  @Security("jwt", ["collection"])
  public async getCollection(
    @Request() request: KoaRequestWithUser,
    @Path() id: number
  ): Promise<HNCollection> {
    await collectionService.verifyCollectionOwnership(id, request.user.sub);
    const collection = await collectionService.getCollection(
      id,
      request.user.sub
    );
    return collection as HNCollection;
  }

  @Delete("/{id}")
  @SuccessResponse("204", "Deleted")
  @Response<ErrorResponse>(401, UnathorizedError.displayName)
  @Security("jwt", ["collection"])
  public async deleteCollection(
    @Request() request: KoaRequestWithUser,
    @Path() id: number
  ): Promise<{}> {
    await collectionService.removeCollection(id, request.user.sub);
    return {};
  }

  @Put("/{id}")
  @SuccessResponse("200", "Updated")
  @Response<ErrorResponse>(401, UnathorizedError.displayName)
  @Response<ErrorResponse>(404, NotFoundError.displayName)
  @Response<ErrorResponse>(403, ForbiddenError.displayName)
  @Security("jwt", ["collection"])
  public async updateCollection(
    @Request() request: KoaRequestWithUser,
    @Path() id: number,
    @Body() body: HNCollectionCreate
  ): Promise<HNCollection> {
    await collectionService.verifyCollectionOwnership(id, request.user.sub);
    return (await collectionService.updateCollection(
      id,
      body,
      request.user.sub
    )) as HNCollection;
  }
}
