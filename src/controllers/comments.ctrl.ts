import {
  Controller,
  Get,
  Path,
  Query,
  Request,
  Response,
  Route,
  Security,
  SuccessResponse,
  Tags,
} from "tsoa";
import {
  CommentFormating,
  ErrorResponse,
  KoaRequestWithUser,
} from "../typings/CustomTsoa";

import * as articleService from "../services/article.service";
import * as commentService from "../services/comment.service";
import * as collectionService from "../services/collection.service";
import {
  ForbiddenError,
  NotFoundError,
  UnathorizedError,
} from "../helpers/error";

@Route("/collections/{collectionId}/articles/{articleId}")
@Tags("comments")
export class CommentController extends Controller {
  @Get("/comments")
  @SuccessResponse("204", "Deleted")
  @Response<ErrorResponse>(401, UnathorizedError.displayName)
  @Response<ErrorResponse>(404, NotFoundError.displayName)
  @Response<ErrorResponse>(403, ForbiddenError.displayName)
  @Security("jwt", ["comment"])
  public async getCollectionArticle(
    @Request() request: KoaRequestWithUser,
    @Path() collectionId: number,
    @Path() articleId: number,
    @Query() commentsFormating: CommentFormating = "list"
  ): Promise<any> {
    await collectionService.verifyCollectionOwnership(
      collectionId,
      request.user.sub
    );
    if (commentsFormating === "pretty") {
      const comments = await commentService.getArticleComments(articleId);
      const article = await articleService.getArticle(articleId);
      return {
        comments: commentService.pretifyCommentsForArticle(
          comments,
          article.hn_id
        ),
      };
    }

    const comments = await commentService.getArticleComments(articleId);
    return { comments: comments };
  }
}
