import * as Sequelize from "sequelize";
import {
  HNArticleAttributes,
  HNArticleInstance,
} from "../../pg_sequelize/Article";
import {
  HNCollectionAttributes,
  HNCollectionInstance,
} from "../../pg_sequelize/Collection";
import {
  HNCommentAttributes,
  HNCommentInstance,
} from "../../pg_sequelize/Comment";
import { UserAttributes, UserInstance } from "../../pg_sequelize/User";

export interface DbInterface {
  sequelize: Sequelize.Sequelize;
  Sequelize: Sequelize.SequelizeStatic;
  Comment: Sequelize.Model<HNCommentInstance, HNCommentAttributes>;
  Article: Sequelize.Model<HNArticleInstance, HNArticleAttributes>;
  User: Sequelize.Model<UserInstance, UserAttributes>;
  Collection: Sequelize.Model<HNCollectionInstance, HNCollectionAttributes>;
}
