import { Request } from "koa";

export interface ErrorResponse {
  statusCode: number;
  statusMessage: string;
  description?: string;
  payload?: string;
}

// Collections

export interface HNCollection {
  id: number;
  name: string;
  createdAt: Date;
  updatedAt?: Date;
}

export interface HNCollectionList {
  collections: HNCollection[];
}

export interface HNCollectionCreate {
  name: string;
}

// Articles TSOA

export interface HNArticleBase {
  id: number;
  hn_id: number;
  title: string;
  author?: string;
  url?: string;
  text?: string;
  createdAt: Date;
  updatedAt?: Date;
}

export interface HNArticle extends HNArticleBase {
  comments?: number[];
}

export interface HNArticleList {
  articles: HNArticleBase[];
}

export interface HNArticleCreate {
  article_ids: number[];
}

// Comments TSOA

export interface HNComment {
  id: number;
  hn_id: number;
  parent_id?: number;
  author?: string;
  text: string;
  kids: number[];
  comments?: HNComment[];
}

// Users TSOA

export interface User {
  id: number;
  name: string;
  createdAt: Date;
  updatedAt?: Date;
}

export interface UserLoginBody {
  name: string;
  password: string;
}

export interface UserLoginResponse {
  jwt: string;
}

// Else

export interface KoaRequestWithUser extends Request {
  user: JWTPayolad;
}

export interface JWTPayolad {
  sub: number;
  iss: string;
  scopes: string[];
}

export type CommentFormating = "list" | "pretty";
