import { InteralServerError, NotFoundError } from "../helpers/error";
import { getInstance } from "../pg_sequelize";
import { HNArticleInstance } from "../pg_sequelize/Article";
import { HNArticle } from "../typings/CustomTsoa";
import { verifyCollectionOwnership } from "./collection.service";

export const getArticles = async (
  collectionId: number
): Promise<HNArticleInstance[]> => {
  const db = getInstance();
  const articles = await db?.Article.findAll({
    include: [
      {
        model: db.Collection,
        where: { id: collectionId },
        // excluding join table from final request
        attributes: [],
        through: {
          attributes: [],
        },
      },
    ],
  });
  if (!articles) {
    return [];
  }
  return articles;
};

export const createArticle = async (
  newArticle: HNArticle,
  collectioId: number
): Promise<HNArticleInstance> => {
  const db = getInstance();
  const collection = await db?.Collection.findOne({
    where: { id: collectioId },
  });
  const article = await db?.Article.findOne({
    where: { hn_id: newArticle.hn_id },
  });
  if (article) {
    const res = await article.update(newArticle);
    return res;
  } else {
    const res = await collection?.createArticle(newArticle);
    if (!res) {
      throw new InteralServerError(
        InteralServerError.displayName,
        "Unable create article instance"
      );
    }
    return res;
  }
};

export const getArticle = async (
  articleId: number
): Promise<HNArticleInstance> => {
  const db = getInstance();
  const article = await db?.Article.findOne({ where: { id: articleId } });
  if (!article) {
    throw new NotFoundError("Not found", "Specified article does not exist");
  }
  return article;
};

export const createMultipleArticles = async (
  newArticles: HNArticle[],
  collectioId: number,
  userId: number
): Promise<HNArticleInstance[]> => {
  // await verifyCollectionOwnership(collectioId, userId);
  const res = await Promise.all(
    newArticles.map(async (article: HNArticle) => {
      return await createArticle(article, collectioId);
    })
  );
  return res;
};

export const removeArticleFromCollection = async (
  articleId: number,
  collectionId: number
) => {
  const article = await getArticle(articleId);
  await article.removeCollection(collectionId);
  return;
};
