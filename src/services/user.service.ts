import { getInstance } from "../pg_sequelize";
import {
  JWTPayolad,
  User,
  UserLoginBody,
  UserLoginResponse,
} from "../typings/CustomTsoa";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { InteralServerError, UnathorizedError } from "../helpers/error";
import { UserInstance } from "../pg_sequelize/User";

export const authorize = async (
  user: UserLoginBody
): Promise<UserLoginResponse> => {
  const db = getInstance();
  const usr = await db?.User.findOne({ where: { name: user.name } });
  if (!usr) {
    throw new UnathorizedError(
      "Unathorized",
      "username or password is incorrect"
    );
  }

  const result = await bcrypt.compare(user.password, usr.password);

  if (!result) {
    throw new UnathorizedError(
      "Unathorized",
      "username or password is incorrect"
    );
  }

  const jwt = await generateJWT(usr);
  return { jwt };
};

export const signUp = async (user: UserLoginBody): Promise<User> => {
  const db = getInstance();
  const hashedPassword = await hashPassword(user.password);
  const newUser = await db?.User.create({
    name: user.name,
    password: hashedPassword,
  });
  if (!newUser) {
    throw new InteralServerError(
      InteralServerError.displayName,
      "Unable create new user"
    );
  }
  return <User>{ id: newUser.id, name: newUser.name };
};

const hashPassword = async (password: string): Promise<string> => {
  const salt = await bcrypt.genSalt();
  const hashedPassword = await bcrypt.hash(password, salt);
  return hashedPassword;
};

export const generateJWT = async (user: UserInstance): Promise<string> => {
  const payload = <JWTPayolad>{
    iss: "hacker-news-api",
    sub: user.id,
    scopes: ["collection", "comment", "article"],
  };
  const token = await jwt.sign(payload, process.env.JWT_SECRET as string, {
    expiresIn: "1d",
  });
  return token;
};
