import fetch from "node-fetch";
import { HttpError } from "../helpers/error";
import { HNArticle, HNComment } from "../typings/CustomTsoa";

const getBaseApi = (): string => {
  return process.env.HACKER_NEWS_BASE_API as string;
};

export const getStory = async (id: number) => {
  try {
    const res = await fetch(`${getBaseApi()}/item/${id}.json`);
    const story = await res.json();

    return <HNArticle>{
      title: story.title,
      author: story.by,
      url: story.url,
      text: story.text,
      comments: story.kids,
      hn_id: story.id,
    };
  } catch (error) {
    throw new HttpError(
      503,
      "Service Unavailable",
      "There was a problem with processing your request"
    );
  }
};

export const getMultipleStories = async (ids: number[]) => {
  try {
    const articles = await Promise.all(
      ids.map(async (id: number) => {
        return await getStory(id);
      })
    );
    return articles;
  } catch (error) {
    throw new HttpError(
      503,
      "Service Unavailable",
      "There was a problem with processing your request"
    );
  }
};

export const getComment = async (id: number) => {
  try {
    const res = await fetch(`${getBaseApi()}/item/${id}.json`);
    const comment = await res.json();

    return <HNComment>{
      author: comment.by,
      text: comment.text,
      kids: comment.kids,
      hn_id: comment.id,
      parent_id: comment.parent,
    };
  } catch (error) {
    throw new HttpError(
      503,
      "Service Unavailable",
      "There was a problem with processing your request"
    );
  }
};

export const getMultipleComments = async (
  ids: number[]
): Promise<HNComment[]> => {
  try {
    const comments = await Promise.all(
      ids.map(async (id: number) => {
        return await getComment(id);
      })
    );

    const kids = comments.reduce((acum: number[], item: HNComment) => {
      if (item.kids) {
        return [...acum, ...item.kids];
      } else return acum;
    }, []);
    if (!kids || kids.length === 0) {
      return comments;
    } else {
      const nextComments = await getMultipleComments(kids);
      return [...comments, ...nextComments];
    }
  } catch (error) {
    throw new HttpError(
      503,
      "Service Unavailable",
      "There was a problem with processing your request"
    );
  }
};
