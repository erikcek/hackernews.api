import {
  ForbiddenError,
  InteralServerError,
  NotFoundError,
} from "../helpers/error";
import { getInstance } from "../pg_sequelize";
import {
  HNCollectionAttributes,
  HNCollectionInstance,
} from "../pg_sequelize/Collection";
import { HNCollectionCreate } from "../typings/CustomTsoa";

export const getCollections = async (
  userId: number
): Promise<HNCollectionInstance[]> => {
  const db = getInstance();
  const res = await db?.Collection.findAll({ where: { UserId: userId } });
  if (!res) {
    return [];
  }
  return res;
};

export const createCollection = async (
  newCollection: HNCollectionAttributes,
  userId: number
): Promise<HNCollectionInstance> => {
  const db = getInstance();
  const user = await db?.User.findOne({ where: { id: userId } });
  if (!user) {
    throw new NotFoundError("User not found");
  }
  const res = await user?.createCollection({ ...newCollection });
  if (!res) {
    throw new InteralServerError(
      InteralServerError.displayName,
      "Unable create collection"
    );
  }
  return res;
};

export const getCollection = async (
  collectionId: number,
  userId: number
): Promise<HNCollectionInstance> => {
  const db = getInstance();
  const res = await db?.Collection.findOne({
    where: { id: collectionId, UserId: userId },
  });
  if (!res) {
    throw new NotFoundError("Collection not found");
  }
  return res;
};

export const removeCollection = async (
  collectionId: number,
  userId: number
): Promise<void> => {
  const db = getInstance();
  await db?.Collection.destroy({
    where: { id: collectionId, UserId: userId },
  });
  // clean up of articles without collection is being done by cron job - see crons directory
  return;
};

export const updateCollection = async (
  collectionId: number,
  newCollection: HNCollectionCreate,
  userId: number
): Promise<HNCollectionInstance> => {
  const collection = await getCollection(collectionId, userId);
  const res = await collection.update(newCollection);
  if (!res) {
    throw new InteralServerError(
      InteralServerError.displayName,
      "Unable update collection"
    );
  }
  return res;
};

// throws forbiden in case of invalid collection claims
export const verifyCollectionOwnership = async (
  collectionId: number,
  userId: number
): Promise<void> => {
  const db = getInstance();
  const user = await db?.User.findOne({ where: { id: userId } });
  if (!user) {
    throw new NotFoundError("Not found", "User Not Found");
  }
  const collection = await db?.Collection.findOne({
    where: { id: collectionId },
  });
  if (!collection) {
    throw new NotFoundError(NotFoundError.displayName, "collection not found");
  }
  const ownership = await user.hasCollection(collectionId);
  if (!ownership) {
    throw new ForbiddenError(
      ForbiddenError.displayName,
      "You are not permited to execute this operation"
    );
  }
};
