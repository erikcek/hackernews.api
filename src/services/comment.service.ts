import { getInstance } from "../pg_sequelize";
import { HNCommentInstance } from "../pg_sequelize/Comment";
import { HNComment } from "../typings/CustomTsoa";
import { getArticle } from "./article.service";

export const createComment = async (
  articleId: number,
  comment: HNComment
): Promise<HNCommentInstance> => {
  const db = getInstance();
  const existingComment = await db?.Comment.findOne({
    where: { hn_id: comment.hn_id },
  });
  const article = await getArticle(articleId);
  if (existingComment) {
    return await existingComment.update(comment);
  }
  const res = await article.createComment(comment);
  return res;
};

export const createMultipleComments = async (
  collectionId: number,
  userId: number,
  articleId: number,
  comments: HNComment[]
): Promise<HNCommentInstance[]> => {
  const res = await Promise.all(
    comments.map(async (singleComment: HNComment) => {
      return await createComment(articleId, singleComment);
    })
  );
  return res;
};

// make recursive parent child structure for articles
export const pretifyCommentsForArticle = (
  comments: HNComment[],
  parentId: number
) => {
  const topComments = comments.filter((f) => f.parent_id === parentId);
  if (topComments.length === 0) {
    return [];
  } else {
  }
  const test: HNComment[] = topComments.reduce(
    (acum: HNComment[], item: HNComment) => {
      if (item.kids) {
        return [
          ...acum,
          {
            ...item,
            comments: pretifyCommentsForArticle(comments, item.hn_id),
          },
        ];
      } else {
        return [...acum, { ...item, comments: [] }];
      }
    },
    []
  );

  return test;
};

// return raw comments (without sequelize instance attributes and methods)
export const getArticleComments = async (
  articleId: number
): Promise<HNComment[]> => {
  const db = getInstance();
  const coments = await db?.Comment.findAll({
    where: { ArticleId: articleId },
    raw: true,
  });
  if (!coments) {
    return [];
  }
  return coments as HNComment[];
};
