import dotenv from "dotenv";
import { connect as pgConnect } from "./pg_sequelize";
import cron from "node-cron";
import {
  removeUnusedArticlesWithComments,
  syncHanckerNewsArticlesJob,
} from "./crons/sync.cron";
import * as winstonLogger from "./helpers/logger";
import { createServer } from "./server";

dotenv.config();

const preconfigure = async () => {
  // creating basic winston logger

  winstonLogger.createWinstonLogger();
  const winstonInstance = winstonLogger.getInstance();

  // connecting to postgress
  try {
    await pgConnect({
      params: {
        host: process.env.PG_HOST,
        dialect: "postgres",
        operatorsAliases: false,
        logging: false,
      },
      database: process.env.PG_DATABASE,
      username: process.env.PG_USERNAME,
      password: process.env.PG_PASSWORD,
    });
  } catch (error) {
    console.log("Unable connect to postrgress");
    console.log("exiting application");
    process.exit(1);
  }

  // registering cron jobs
  // registering both cronjobs for every hour - 0 * * * *
  // if you want to increate granularity change cron settings
  // example: every minute = */1 * * * *
  const syncArticlesJob = cron.schedule("0 * * * *", async () => {
    try {
      winstonInstance?.info("[CRON] - Starting cron job for article ");
      await syncHanckerNewsArticlesJob();
      winstonInstance?.info("[CRON] - Finished article synchronization");
    } catch (error) {
      winstonInstance?.info({
        message: "Error while synchonizing articles",
        payload: error,
      });
    }
  });

  const cleanUpArticleJob = cron.schedule("0 * * * *", async () => {
    try {
      winstonInstance?.info("[CRON] - Starting cron job for article cleanup ");
      await removeUnusedArticlesWithComments();
      winstonInstance?.info("[CRON] - Finished article cleanup");
    } catch (error) {
      winstonInstance?.info({
        message: "Error while cleaning up articles with comments",
        payload: error,
      });
    }
  });

  cleanUpArticleJob.start();
  syncArticlesJob.start();
};

preconfigure().then((res) => {
  console.log(`Starting server...`);
  createServer();
  console.log(`Server is listening on port ${process.env.PORT}`);
});
