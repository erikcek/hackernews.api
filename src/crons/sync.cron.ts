import { Op } from "sequelize";
import { getInstance } from "../pg_sequelize";
import * as hackerNewsService from "../services/hackerNews.service";
import { HNComment } from "../typings/CustomTsoa";

export const syncHanckerNewsArticlesJob = async () => {
  const db = getInstance();
  const now = new Date();
  const hourBeforeDate = now.setHours(now.getHours() - 0);
  const articles = await db?.Article.findAll({
    where: { updatedAt: { [Op.lt]: hourBeforeDate } },
  });

  if (!articles) {
    return;
  }

  await Promise.all(
    articles.map(async (articleForUpdate) => {
      const article = await hackerNewsService.getStory(articleForUpdate.hn_id);
      const localArticle = await db?.Article.findOne({
        where: { hn_id: article.hn_id },
      });
      if (!localArticle) {
        return;
      }
      await localArticle.update(article);
      if (article.comments) {
        const comments = await hackerNewsService.getMultipleComments(
          article.comments
        );

        await Promise.all(
          comments.map(async (comment: HNComment) => {
            const localComment = await db?.Comment.findOne({
              where: { hn_id: comment.hn_id },
            });
            if (!localComment) {
              return await localArticle.createComment(comment);
            } else {
              return await localComment.update(comment);
            }
          })
        );
      }
    })
  );
};

export const removeUnusedArticlesWithComments = async () => {
  const db = getInstance();
  const articles = await db?.Article.findAll();
  if (!articles) {
    return;
  }

  await Promise.all(
    articles.map(async (article) => {
      const collections = await article.getCollections();

      if (collections.length === 0) {
        const comments = await article.getComments();
        const commentIds = comments.map((comment) => comment.id);
        await db?.Comment.destroy({ where: { id: commentIds } });
        await article.destroy();
      }
    })
  );
};
