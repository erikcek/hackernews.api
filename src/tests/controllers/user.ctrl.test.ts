import request from "supertest";
import { closeConnection, connect } from "../../pg_sequelize";
import { createServer } from "../../server";
import { DbInterface } from "../../typings/DbInterface";
import dotenv from "dotenv";
import { UserInstance } from "../../pg_sequelize/User";

describe("User controller test", () => {
  const state: {
    server?: any;
    db?: DbInterface;
    userPassword: string;
    userName: string;
  } = {
    userName: "NotExistingUserForTesting",
    userPassword: "NotExistingUserForTesting",
  };

  it("shout not find user and return unathorized", async (done) => {
    request(state.server)
      .post("/v1/users/authorize")
      .send({
        name: state.userName,
        password: state.userPassword,
      })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(401, done);
  });

  it("shoul sign up user", async (done) => {
    request(state.server)
      .post("/v1/users/signUp")
      .send({
        name: state.userName,
        password: state.userPassword,
      })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });

  it("shout find user and return payload with jwt token", async (done) => {
    request(state.server)
      .post("/v1/users/authorize")
      .send({
        name: state.userName,
        password: state.userPassword,
      })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });

  beforeAll(async () => {
    dotenv.config();
    const db = await connect({
      params: {
        host: "localhost",
        dialect: "postgres",
        operatorsAliases: false,
        logging: false,
      },
      database: process.env.PG_DATABASE,
      username: process.env.PG_USERNAME,
      password: process.env.PG_PASSWORD,
    });

    state.db = db;
    state.server = createServer();
  });

  afterAll(async () => {
    state.server.close();
    await state.db?.User.destroy({
      where: { name: state.userName },
    });
    await closeConnection();
  });
});
