import request from "supertest";
import { closeConnection, connect } from "../../pg_sequelize";
import { createServer } from "../../server";
import { DbInterface } from "../../typings/DbInterface";
import dotenv from "dotenv";
import { UserInstance } from "../../pg_sequelize/User";
import * as userService from "../../services/user.service";

describe("Collection controller test", () => {
  const state: {
    server?: any;
    db?: DbInterface;
    user?: UserInstance;
    jwt?: string;
    newCollectionId?: string;
  } = {};

  it("POST /v1/collections/  -  should return unathorized", async (done) => {
    request(state.server)
      .post("/v1/collections/")
      .send({ incorectName: "test" })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(401, done);
  }, 10000);

  it("POST /v1/collections/  -  should return 404 error", async (done) => {
    request(state.server)
      .post("/v1/collections/")
      .send({ incorectName: "test" })
      .set("Accept", "application/json")
      .set("Authorization", `Bearer ${state.jwt}`)
      .expect("Content-Type", /json/)
      .expect(400, done);
  }, 10000);

  it("POST /v1/collections/  -  should create collection", async (done) => {
    request(state.server)
      .post("/v1/collections/")
      .send({ name: "testCollectionName" })
      .set("Accept", "application/json")
      .set("Authorization", `Bearer ${state.jwt}`)
      .expect("Content-Type", /json/)
      .expect(201)
      .then((response) => {
        state.newCollectionId = response.body.id;
        done();
      });
  }, 10000);

  it("GET /v1/collections/  -  should return Unauthorized error", async (done) => {
    request(state.server)
      .get("/v1/collections/")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(401, done);
  }, 10000);

  it("GET /v1/collections/  -  should return one collection", async (done) => {
    request(state.server)
      .get("/v1/collections/")
      .set("Accept", "application/json")
      .set("Authorization", `Bearer ${state.jwt}`)
      .expect("Content-Type", /json/)
      .expect(200, done);
  }, 10000);

  it("GET /v1/collections/:id  -  should return Unauthorized error", async (done) => {
    request(state.server)
      .get(`/v1/collections/${state.newCollectionId}`)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(401, done);
  }, 10000);

  it("GET /v1/collections/:id  -  should return one collection", async (done) => {
    request(state.server)
      .get(`/v1/collections/${state.newCollectionId}`)
      .set("Authorization", `Bearer ${state.jwt}`)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  }, 10000);

  it("PUT /v1/collections/:id  -  should return Unathorized error", async (done) => {
    request(state.server)
      .put(`/v1/collections/${state.newCollectionId}`)
      .send({ name: "new_test_name" })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(401, done);
  }, 10000);

  it("PUT /v1/collections/:id  -  should update collection", async (done) => {
    request(state.server)
      .put(`/v1/collections/${state.newCollectionId}`)
      .send({ name: "new_test_name" })
      .set("Authorization", `Bearer ${state.jwt}`)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  }, 10000);

  it("DELETE /v1/collections/:id  -  should return Unauthorized error", async (done) => {
    request(state.server)
      .delete(`/v1/collections/${state.newCollectionId}`)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(401, done);
  }, 10000);

  it("DELETE /v1/collections/:id  -  should delete specified collection", async (done) => {
    request(state.server)
      .delete(`/v1/collections/${state.newCollectionId}`)
      .set("Authorization", `Bearer ${state.jwt}`)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  }, 10000);

  it("GET /v1/collections/:id  -  should return Not Found Error", async (done) => {
    request(state.server)
      .get(`/v1/collections/${state.newCollectionId}`)
      .set("Authorization", `Bearer ${state.jwt}`)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(404, done);
  }, 10000);

  beforeAll(async () => {
    dotenv.config();
    const db = await connect({
      params: {
        host: "localhost",
        dialect: "postgres",
        operatorsAliases: false,
        logging: false,
      },
      database: process.env.PG_DATABASE,
      username: process.env.PG_USERNAME,
      password: process.env.PG_PASSWORD,
    });

    const user = await db?.User.create({
      name: "testUser",
      password: "testPassword",
    });

    const jwt = await userService.generateJWT(user as UserInstance);

    state.jwt = jwt;
    state.user = user;
    state.db = db;
    state.server = createServer();
  });

  afterAll(async () => {
    state.server.close();

    await state.user?.destroy();

    await closeConnection();
  });
});
