FROM node:alpine

WORKDIR /usr/src/app

COPY ["package.json", "package-lock.json*", "yarn.lock", "./"]

RUN yarn

COPY . .

RUN yarn build

CMD [ "yarn", "start" ]